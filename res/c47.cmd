cd c:\C47
start "" "c47.exe"

rem Examples of command line parameters
rem start "" "c47.exe" "--functionkeys"
rem start "" "c47.exe" "--c47" "--auto"
rem start "" "c47.exe" "--c47" "--portrait" "--functionkeys"
rem start "" "c47.exe" "--c47" "--landscape" "--functionkeys"
rem start "" "c47.exe" "--r47v0" "--auto" "--functionkeys"
rem start "" "c47.exe" "--r47v1" "--auto" "--functionkeys"
rem start "" "c47.exe" "--r47v2" "--auto" "--functionkeys" "--background" "R47custom.png"
