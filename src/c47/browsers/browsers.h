// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file browsers.h
 ***********************************************/
#if !defined(BROWSERS_H)
  #define BROWSERS_H

  #include "asnBrowser.h"
  #include "flagBrowser.h"
  #include "fontBrowser.h"
  #include "registerBrowser.h"
#endif // !BROWSERS_H
