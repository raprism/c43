// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file xor.h
 ***********************************************/
#if !defined(XOR_H)
  #define XOR_H

  void fnLogicalXor(uint16_t unusedButMandatoryParameter);
#endif // !XOR_H
