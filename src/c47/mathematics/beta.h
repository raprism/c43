// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file beta.h
 ***********************************************/
#if !defined(BETA_H)
  #define BETA_H

  void fnBeta      (uint16_t unusedButMandatoryParameter);
#endif // !BETA_H
