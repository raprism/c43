// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file erf.h
 ***********************************************/
#if !defined(ERF_H)
  #define ERF_H

  void fnErf   (uint16_t unusedButMandatoryParameter);

#endif // !ERF_H
