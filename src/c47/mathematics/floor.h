// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file floor.h
 ***********************************************/
#if !defined(FLOOR_H)
  #define FLOOR_H

  void fnFloor   (uint16_t unusedButMandatoryParameter);
#endif // !FLOOR_H
