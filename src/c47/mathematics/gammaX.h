// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file gammaX.h
 ***********************************************/
#if !defined(GAMMAP_X)
  #define GAMMAP_X

  void fnGammaX      (uint16_t gammaType);
#endif // !GAMMAP_X
