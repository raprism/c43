// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file linpol.h
 ***********************************************/

#if !defined(LINPOL_H)
  #define LINPOL_H

  void linpol(const real_t *a, const real_t *b, const real_t *p, real_t *res);

  void fnLINPOL             (uint16_t unusedButMandatoryParameter);

#endif // !LINPOL_H
