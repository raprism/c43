// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file percentPlusMG.h
 ***********************************************/
#if !defined(PERCENTPLUSMG_H)
  #define PERCENTPLUSMG_H

  void fnPercentPlusMG(uint16_t unusedButMandatoryParameter);

  void percentPlusMGLonILonI(void);
  void percentPlusMGLonIReal(void);
  void percentPlusMGRealLonI(void);
  void percentPlusMGRealReal(void);
#endif // !PERCENTPLUSMG_H
