// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

/********************************************//**
 * \file sign.h
 ***********************************************/
#if !defined(SIGN_H)
  #define SIGN_H

  void fnSign   (uint16_t unusedButMandatoryParameter);
#endif // !SIGN_H
