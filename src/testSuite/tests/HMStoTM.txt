;*************************************************************
;*************************************************************
;**                                                         **
;**                         -> HMS to TIME TYPE             **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RMODE=0 IM=2compl SS=4 WS=64
Func: fnHMStoTM



;=======================================
; HMStoTM(Long Integer) --> Time
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"12"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"12" RX=Time:"12:00:00"



;=======================================
; HMStoTM(Time) --> Time
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Time:"12:34:56.789"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Time:"12:34:56.789" RX=Time:"12:34:56.789"



;=======================================
; HMStoTM(Date) --> Error24
;=======================================
In:  FL_YMD=1 FL_MDY=0 FL_DMY=0 JG=2299161 FL_ASLIFT=0 FL_CPXRES=0 RX=Date:"1582.1015"
Out: FL_YMD=1 FL_MDY=0 FL_DMY=0 JG=2299161 EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Date:"1582.1015"



;=======================================
; HMStoTM(String) --> Error24
;=======================================
In:  FL_ASLIFT=0 RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; HMStoTM(Real Matrix) --> Error24
;=======================================



;=======================================
; HMStoTM(Complex Matrix) --> Error24
;=======================================



;=======================================
; HMStoTM(Short Integer) --> Error24
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 WS=64 IM=2compl RX=ShoI:"5#7"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=ShoI:"5#7"


;=======================================
; HMStoTM(Real) --> Time
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"12.3456789"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"12.3456789" RX=Time:"12:34:56.789"

In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"12.3456789":DEG
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"12.3456789":DEG



;=======================================
; HMStoTM(Complex) --> Error24
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"6.2 i -7.6"
Out: EC=24 FL_ASLIFT=0 RX=Cplx:"6.2 i -7.6"
