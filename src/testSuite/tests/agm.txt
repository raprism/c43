;*************************************************************
;*************************************************************
;**                                                         **
;**                         agm                             **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RMODE=0 IM=2compl SS=4 WS=64
Func: fnAgm

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"5" RY=CxMa:"M2,1[1i0,2i2]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Real:"5" RX=CxMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446i0,3.490364224258774600774461907616461 i 1.218849344771390402135500844485586]"

;=======================================================================================================================
; Error case: reals and x<0 or y<0 or NaN or wrong data types
;=======================================================================================================================

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-2" RY=Real:"5"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"-2" RY=Real:"5"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"5" RY=Real:"-2"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"5" RY=Real:"-2"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"-2" RY=LonI:"5"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=LonI:"-2" RY=LonI:"5"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"5" RY=LonI:"-2"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=LonI:"5" RY=LonI:"-2"

;=======================================================================================================================
; Integer Domain
;=======================================================================================================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=ShoI:"1#10" RY=LonI:"5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=ShoI:"1#10" RX=Real:"2.60400819053094028869642744872503433092706525563764948774841325446"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"1" RY=ShoI:"5#10"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"1" RX=Real:"2.60400819053094028869642744872503433092706525563764948774841325446"

;=======================================================================================================================
; Real Domain
;=======================================================================================================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"1" RY=Real:"5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"1" RX=Real:"2.60400819053094028869642744872503433092706525563764948774841325446"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"1000" RY=Real:"1000000"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"1000" RX=Real:"189388.302409950875557668383899002284150743147627494539244754636078"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"0.1" RY=Real:"0.00000001"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"0.00897372788032617635832975712477846117577814780155175563194533968106"

; Special case fast exist zeros
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"0" RY=Real:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"1" RY=Real:"0"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"1" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-1" RY=Real:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-1" RX=Real:"0"


;=======================================================================================================================
; Complex Domain
;=======================================================================================================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"1" RY=Cplx:"5 i 1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=LonI:"1" RX=Cplx:"2.61069957265504884786303570933994530375554191028876184973777910267 i 0.352968798877474658674656438810498154770461336822115251643519498869"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"5 i 1" RY=LonI:"1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"5 i 1" RX=Cplx:"2.61069957265504884786303570933994530375554191028876184973777910267 i 0.352968798877474658674656438810498154770461336822115251643519498869"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"-9 i 7" RY=Cplx:"5 i -3"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-9 i 7" RX=Cplx:"0.446661494414316213845753588088614523219384054035919850513847328 i 4.61291646480734130018191170975979034279471855163665097838831909"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"-3 i 1" RY=Cplx:"2 i -4"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-3 i 1" RX=Cplx:"-1.34323728275499834180689073804758426619513929735435247559538078 i -2.14560116837819481989757538021062721857903966929693016552957615"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=LonI:"-2" RY=LonI:"5"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=LonI:"-2" RX=Cplx:"1.135078366460119596718762687873972 i 1.578058852302749068410087568838160"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=LonI:"5" RY=LonI:"-2"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=LonI:"5" RX=Cplx:"1.135078366460119596718762687873972 i 1.578058852302749068410087568838160"

;=======================================================================================================================
; Matrix / Number
;=======================================================================================================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=ReMa:"M2,1[1,2]" RY=Real:"5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=ReMa:"M2,1[1,2]" RX=ReMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446,3.328997143273107129201897476670707]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"5" RY=ReMa:"M2,1[1,2]"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"5" RX=ReMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446,3.328997143273107129201897476670707]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"5" RY=CxMa:"M2,1[1i0,2i2]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Real:"5" RX=CxMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446i0,3.490364224258774600774461907616461 i 1.218849344771390402135500844485586]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=CxMa:"M2,1[1,2i2]" RY=Real:"5"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=CxMa:"M2,1[1,2i2]" RX=CxMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446i0,3.490364224258774600774461907616461 i 1.218849344771390402135500844485586]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=ReMa:"M2,1[1,2]" RY=Cplx:"5 i 1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=ReMa:"M2,1[1,2]" RX=CxMa:"M2,1[2.61069957265504884786303570933994530375554191028876184973777910267 i 0.352968798877474658674656438810498154770461336822115251643519498869,3.337399356266944304267189752596781 i 0.4052885730163476568828551215784967]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"5 i 1" RY=ReMa:"M2,1[1,2]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"5 i 1" RX=CxMa:"M2,1[2.61069957265504884786303570933994530375554191028876184973777910267 i 0.352968798877474658674656438810498154770461336822115251643519498869,3.337399356266944304267189752596781 i 0.4052885730163476568828551215784967]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=CxMa:"M2,1[1,2]" RY=Cplx:"5 i 1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=CxMa:"M2,1[1,2]" RX=CxMa:"M2,1[2.61069957265504884786303570933994530375554191028876184973777910267 i 0.352968798877474658674656438810498154770461336822115251643519498869,3.337399356266944304267189752596781 i 0.4052885730163476568828551215784967]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"5 i 1" RY=CxMa:"M2,1[1,2]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"5 i 1" RX=CxMa:"M2,1[2.61069957265504884786303570933994530375554191028876184973777910267 i 0.352968798877474658674656438810498154770461336822115251643519498869,3.337399356266944304267189752596781 i 0.4052885730163476568828551215784967]"

;=======================================================================================================================
; Matrix / Matrix
;=======================================================================================================================

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=ReMa:"M2,1[1,2]" RY=ReMa:"M2,1[5,6]"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=ReMa:"M2,1[1,2]" RX=ReMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446,3.727233566489793084711378062048541]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=ReMa:"M2,1[1,2]" RY=CxMa:"M2,1[5,6]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=ReMa:"M2,1[1,2]" RX=CxMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446,3.727233566489793084711378062048541i0]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=CxMa:"M2,1[1,2]" RY=ReMa:"M2,1[5,6]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=CxMa:"M2,1[1,2]" RX=CxMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446,3.727233566489793084711378062048541i0]"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=CxMa:"M2,1[1,2]" RY=CxMa:"M2,1[5,6]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=CxMa:"M2,1[1,2]" RX=CxMa:"M2,1[2.60400819053094028869642744872503433092706525563764948774841325446,3.727233566489793084711378062048541i0]"
